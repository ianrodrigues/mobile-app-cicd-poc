#!/usr/bin/env sh

set -ex

NEXUS_ENV="development"

if [ ! -z $CI_COMMIT_TAG ]; then
    NEXUS_ENV="production"
fi

echo "Uploading to Nexus: ${NEXUS_ENV} ..."
