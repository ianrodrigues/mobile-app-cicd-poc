#!/usr/bin/env sh

set -ex

MANIFEST_VERSION="${CI_COMMIT_SHORT_SHA}-SNAPSHOT"

if [ ! -z $CI_COMMIT_TAG ]; then
    MANIFEST_VERSION="${CI_COMMIT_TAG}"
fi

echo "Packaging version ${MANIFEST_VERSION} ..."
