#!/usr/bin/env sh

set -ex

AWS_ENV="development"

if [ ! -z $CI_COMMIT_TAG ]; then
    AWS_ENV="production"
fi

echo "Uploading to AWS: ${AWS_ENV} ..."
